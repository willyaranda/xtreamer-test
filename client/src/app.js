import React from 'react';
import { Route } from 'react-router-dom';
import Header from './components/header';
import MovieList from './components/movie_list';
import MovieView from './components/movie_single_view';

const App = () => (
  <div>
    <Header />
    <Route exact path="/" component={MovieList} />
    <Route exact path="/movie/:id" component={MovieView} />
  </div>
);

export default App;
