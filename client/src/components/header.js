import React from 'react';
import SearchBar from 'material-ui-search-bar';

import '../../res/scss/header.scss';


const Header = () => (
  <div style={{ marginTop: 20 }} className="header">
    <div className="left">
      <ul className="primary">
        <li className="logo">
          <a href="/">
            <img src="https://www.themoviedb.org/static_cache/v4/logos/primary-green-d70eebe18a5eb5b166d5c1ef0796715b8d1a2cbc698f96d311d62f894ae87085.svg" width="91" height="81" />
          </a>
        </li>
        <li>
          <a href="/discover">Descubre</a>
        </li>
        <li>
          <a href="/movie">Películas</a>
        </li>
        <li>
          <a href="/tv">Series</a>
        </li>
        <li>
          <a href="/person">Gente</a>
        </li>

      </ul>
    </div>
    <SearchBar hintText="Buscar película…" onChange={e => console.log(`onchange ${e}`)} onRequestSearch={e => console.log(`onRequestSearch ${e}`)} />
  </div>
);

export default Header;
