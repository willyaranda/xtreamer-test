import React from 'react';
import MovieListGrid from './movie_list_grid';

const Home = () => (
  <div className="home">
    <h1>Películas populares</h1>
    <MovieListGrid />
  </div>
);

export default Home;
