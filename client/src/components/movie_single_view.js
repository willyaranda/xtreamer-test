import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import numeral from 'numeral';

class MovieSingleView extends Component {
  constructor(props) {
    super(props);
    this.state = { movie: null };
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    this.getMovie(id);
  }

  async getMovie(id) {
    const res = await fetch(`http://localhost:3000/api/movies/${id}`);
    const json = await res.json();
    this.setState({ movie: json });
  }

  render() {
    const movie = this.state.movie;
    if (movie == null) {
      return (<h1>{'Loading…'}</h1>);
    }

    const date = moment(movie.release_date, 'YYYY-MM-DD');

    return (
      <div>
        <h1>{movie.original_title}</h1>
        <p>{movie.title} ({date.format('YYYY')})</p>
        <p>{movie.vote_average * 10}% Puntuación de usuario</p>
        <p>General</p>
        <p>{movie.overview}</p>
        <img src={`https://image.tmdb.org/t/p/w185/${movie.poster_path}`} alt={`${movie.title} poster`} />
        <h2>Datos</h2>
        <p>Título original: {movie.original_title}</p>
        <p>Estatus: {movie.status}</p>
        <p>Información estreno: {movie.production_countries.map(c => c.iso_3166_1).join(', ')} {date.format('LL')}</p>
        <p>Adulto: {movie.adult ? 'Sí' : 'No' }</p>
        <p>Duración: {movie.runtime} minutos</p>
        <p>Presupuesto: {numeral(movie.budget).format('$0,0')}</p>
        <p>Ingresos: {numeral(movie.revenue).format('$0,0')}</p>
        <p>Inicio: <a href={movie.homepage} target="_blank">{movie.homepage}</a></p>
        <p>Géneros: {movie.genres.map(g => g.name).join(', ')}</p>
      </div>
    );
  }
}


MovieSingleView.propTypes = {
  match: PropTypes.object.isRequired,
};

export default MovieSingleView;
