import React, { Component } from 'react';

import MovieWidget from './movie_widget';

class MovieListGrid extends Component {
  constructor(props) {
    super(props);
    this.state = { movies: { results: [] } };
  }

  componentDidMount() {
    this.getMovies();
  }

  async getMovies() {
    const res = await fetch('http://localhost:3000/api/movies');
    const json = await res.json();
    this.setState({ movies: json });
  }

  renderMovieWidget() {
    const { movies } = this.state;

    return movies.results.map(movie => (
      <li key={`movie-${movie.id}`}><MovieWidget movie={movie} /></li>
    ));
  }

  render() {
    return (
      <div className="list_items">
        <ul>
          { this.renderMovieWidget() }
        </ul>
      </div>
    );
  }
}

export default MovieListGrid;
