import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import moment from 'moment';

import { Circle } from 'rc-progress';

import '../../res/scss/widget.scss';

// vote_count, id, video, vote_average, title, popularity, poster_path,
// original_language, original_title, genre_ids,
// backdrop_path, adult, overview, release_date

const MovieWidget = ({ movie }) => (
  <div className="movie_widget">
    <div className="movie_widget_left">
      <img src={`https://image.tmdb.org/t/p/w185/${movie.poster_path}`} alt={`${movie.title} poster`} />
    </div>
    <div className="movie_widget_right">
      <div className="movie_widget_right_up">
        <div className="movie_widget_header_wrapper">
          <div>
            <Circle className="movie_widget_percentage" percent={movie.vote_average * 10} strokeWidth="20" strokeColor="green" />
          </div>
          <div className="movie_widget_header">
            <p>{movie.title}</p>
            <p>{moment(movie.release_date, 'YYYY-MM-DD').format('LL')}</p>
          </div>
        </div>
        <p>{movie.overview}</p>
      </div>
      <div className="movie_widget_right_down">
        <Link to={`/movie/${movie.id}`}>
          <button type="button">Más información</button>
        </Link>
      </div>
    </div>
  </div>
);

MovieWidget.propTypes = {
  movie: PropTypes.object.isRequired,
};

export default MovieWidget;
