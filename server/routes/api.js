import express from 'express';
import mdb from '../lib/moviedb';

const router = express.Router();

router.get('/movies', async (req, res) => {
  try {
    const movies = await mdb.getMovieDiscover();
    res.json(movies);
  } catch (e) {
    if (e.message === 'Not Found') {
      res.status(404);
    } else {
      res.status(500);
    }
    res.end();
  }
});

router.get('/movies/:id', async (req, res) => {
  try {
    const movie = await mdb.getMovieInfo(req.params.id);
    res.json(movie);
  } catch (e) {
    if (e.message === 'Not Found') {
      res.status(404);
    } else {
      res.status(500);
    }
    res.end();
  }
});

export default router;
