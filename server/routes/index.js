import express from 'express';
// import ssr from './ssr';
import moviedb from './api';

const cors = require('cors');

const app = express();
app.use(cors());

app.set('view engine', 'ejs');

app.use(express.static('public'));

// app.use('/*', ssr);
app.use('/api', moviedb);

app.listen(3000, () => {
  console.log('API on port 3000!');
});
