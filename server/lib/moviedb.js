const MovieDB = require('moviedb')('c5e4a1733c2995102fafe209c014e4c0');
const { promisify } = require('util');

const getMovieInfoPr = promisify(MovieDB.movieInfo.bind(MovieDB));
const discoverMoviePr = promisify(MovieDB.discoverMovie.bind(MovieDB));

const getMovieDiscover = async skip => discoverMoviePr();

const getMovieInfo = async id => getMovieInfoPr({ id });

module.exports = {
  getMovieDiscover,
  getMovieInfo,
};
